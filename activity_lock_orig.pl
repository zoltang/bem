#!perl 

#use strict;
use Net::SMTP;

my $SUBJECT       = "ClearCase Delivery Notification: $ENV{'CLEARCASE_DLVR_ACTS'}";
my @EMAIL_TO_LIST = ("z.galgavolgyi\@cmcmarkets.com","");
my $SMTP_SERVER   = "dev-relay-1";
my $EMAIL_DOMAIN  = "CMC.LOCAL";

if ( $ENV{'ATRIA_WEB_GUI'} eq "1" ) {
$promote_home = '\\\\dev-clearcase-1\\Triggers\\';
$quote = "\"";
$slash = "\\";
}

my $IVIEW=$ENV{CLEARCASE_VIEW_TAG}; 		# Integration view to which deliver is being done
my $PROJECT=$ENV{CLEARCASE_PROJECT};
my $ISTREAM  = $ENV{CLEARCASE_STREAM};
my $REC_STREAM = $ENV{CLEARCASE_STREAM};
my $SRCSTREAM=$ENV{CLEARCASE_SRC_STREAM};
my $u_name=$ENV{'USERNAME'};

$OP      = $ENV{CLEARCASE_OP_KIND};
$COM     = $ENV{CLEARCASE_COMMENT};
$CC_VIEW  = $ENV{CLEARCASE_VIEW_TAG};
$USER     = $ENV{CLEARCASE_USER};
$EL_TYPE = $ENV{CLEARCASE_ELTYPE};
$ACTS = $ENV{CLEARCASE_DLVR_ACTS};

$PROJECT=~s/(^.+)(@.+$)/$1/;
$ISTREAM=~s/(^.+)(@.+$)/$1/;
$SRCSTREAM=~s/(^.+)(@.+$)/$1/;
$CT="cleartool";


#my $date=GetCurrentDate();			# Date in form YYYYMMDD_HHMMSS

actlock ();
sendmail ();

sub sendmail {

my @MSG = 
("ClearCase Delivery Notification ($ENV{'CLEARCASE_DLVR_ACTS'}):

	user:   $ENV{'CLEARCASE_USER'}
	operation:   $ENV{'CLEARCASE_OP_KIND'}
	date:   $DATE_TIME 
	element:   $ENV{'CLEARCASE_PN'}
	version:   $ENV{'CLEARCASE_ID_STR'}
	comment:   $ENV{'CLEARCASE_COMMENT'}
	activities: $ENV{'CLEARCASE_DLVR_ACTS'}\n");

#############################################

my $smtp;
   
$smtp = Net::SMTP->new($SMTP_SERVER);

################################################################
# Assuming that user id is a valid email prefix for the domain #
# if not then adjust or hardcode (i.e. vobadm@company.com).    #
################################################################

$smtp->mail("cc\@cc.com");

$smtp->to(@EMAIL_TO_LIST);

$smtp->data();

$smtp->datasend("To: @EMAIL_TO_LIST\n");
$smtp->datasend("Subject: $SUBJECT\n");
$smtp->datasend(@MSG);

$smtp->dataend();

$smtp->quit;
}

sub actlock
{
	@activites = split (/\s/,$ACTS);
	foreach $ACT ( @activites ) {
	`cleartool lock activity:$ACT`;
	}
}


