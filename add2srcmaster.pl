######################################################
# add2srcmaster.pl (Irvine)                          #
# Create an element on the main line of a replicated #
# vob (non master site)                              #
# Author: Steve Stephenson                           #
# email:  sstephen@broadcom.com                      #
######################################################
unshift(@INC,'\\\\SJCLEARCASE\\ccs_d\\Lib\\');
require BRCM::brcmlib;

my $tmplogmaster =  "P:\\tmplogmaster.txt";
my $logMastership = "P:\\logmaster.txt";
my $ReqMaster =     "\\\\Sjclearcase\\ccs_d\\ReqMastership\\Win\\ReqMastership.exe";
my $USERNAME =      getlogin;
my $MASTERSHIP;
my $FILE_PN;
my $ischeckout;
my $failflag;
my $Update_Done = 0;
my $replica;
my $Pwd;
my $i;

for ($i=0; $i <= $#ARGV; $i++){
  $FILE_PN = $ARGV[$i];
  $Pwd = `cleartool pwd`;
  if (-f $FILE_PN && -z $FILE_PN){
     brcmlib::Prompt("*** ERROR: $FILE_PN is zero in size");
     exit(1);
  };
##
## Check the Locked_By Attribute in the Current (Parent) Directory
##
if (-d $FILE_PN ){
   my $Pwd = `cleartool pwd`;
   system("clearprompt proceed -mask proceed -prompt \"Please make sure that the parent directory ($Pwd) is Checked Out\"");
   brcmlib::Prompt("*** Add in directory ($FILE_PN) *** ");
} else {
#- Checked Out (Unreserved) the Current Directory
#- This check will only work in the current View
   $Pwd = ".";
   $ischeckout = `cleartool describe -fmt "%Rf" $Pwd`;
   if (!$ischeckout){
   #- Creating File Elements -- Checkout Parent Directory
      $Pwd = ".";
   #- Add IR_Locked_By attribute if it is not there
      brcmlib::Add_Attr($Pwd,"IR_Locked_By");
   #- Check if SJ_Locked
   	$fileversion = brcmlib::Find_Site_Ver_Attr($Pwd,"SJ_Locked_By","NULL");
   	if (!$fileversion){
   		$fileversion = brcmlib::Find_Ver_NOT_Attr($Pwd,"SJ_Locked_By","NULL");
   		if ($fileversion){
      			## Go Get User Info to Display to Current USER
      			## >cleartool describe -aattr SJ_Locked_By -fmt "%Sa" ##
      			brcmlib::Prompt("Parent Directory ($Pwd) has been checked out by other Irvine developer!");
      			exit(1);
		};
      };
   #- Check if IR_Locked
      $fileversion = brcmlib::Find_Site_Ver_Attr($Pwd,"IR_Locked_By","NULL");
      if (!$fileversion){
        #- Go Get User Info to Display to Current USER
        #- >cleartool describe -aattr IR_Locked_By -fmt "%Sa" ##
         brcmlib::Prompt("*** ERROR: Parent Directory ($Pwd) has been checked out by other Irvine developer!");
         exit(1);
      };
##
##    Check if the view involved is a snapshot view
##
      if (brcmlib::Is_SnapshotView()){
##
##   If the current view is a snapshot use the checkout with ndata, unreserved to suppress errors 
##   regardless of the directory not being the latest
##   Providing the ability to add new elements to the directory without updating old elements 
##     
         $failflag = system("cleartool co -c \"Adding new element to replica\" -unreserved -nmaster -nwarn -ndata .");    
         if ($failflag){
            brcmlib::Prompt("*** ERROR ($failflag): at co -unreserved -nmaster -nwarn -ndata  . ");
                exit(1);
             };
     };
     if (!brcmlib::Is_SnapshotView()){
##
##  Check Out the Current Directory without the ndata argument but unreserved for a dynamic view
##
            $failflag = system("cleartool co -c \"Adding new element to replica\" -unreserved -nmaster .@@/main");
            if ($failflag){
                brcmlib::Prompt("*** ERROR ($failflag): at co -unreserved -nmaster . @@/main");
                exit(1);
            };
    };
##
## Modify IR_Locked_By Attribute on Parent's Directory
##
      $failflag = system("cleartool mkattr -replace -nc IR_Locked_By \\\"$USERNAME\\\" $fileversion");
      if ($failflag){
         brcmlib::Prompt("*** ERROR ($failflag): at Update IR_Locked_By on parent directory");
         exit(1);
      };
##
## Check the Mastership of the Current Directory
##
      $failflag = system("multitool describe -fmt \"%[master]p\" .@@/main > $tmplogmaster");
      if ($failflag){
         brcmlib::Prompt("*** ERROR ($failflag): Get the mastership of the current directory");
         exit(1);
      };
      open(LOGFILE,$tmplogmaster)|| die "*** ERROR: Can not open $tmplogmaster file\n";
      $MASTERSHIP = <LOGFILE>;
      close(LOGFILE);
      $MASTERSHIP =~ s/\@.*//;
##
## Check the VOB-Tag of the Branch
##
      $failflag = system("multitool describe -short vob:. > $tmplogmaster");
      if ($failflag){
         brcmlib::Prompt("*** ERROR ($failflag): Get the VOB-tag");
         exit(1);
      };
      open(LOGFILE,$tmplogmaster)|| die "*** ERROR: Can not open $tmplogmaster file\n";
      $VOB_PN = <LOGFILE>;
      close(LOGFILE);

      $failflag = system("cleartool describe -fmt %[replica_name]p vob:. > $logMastership");
      if ($failflag){
         Prompt("*** ERROR $VOB_PN -- Describe COMMAND FAILED");
         system("del /F $logMastership");
         exit(1);
      };
      open(LOGFILE,$logMastership)|| die "*** ERROR: Can not open $logMastership file\n";
      $replica = <LOGFILE>;
      close(LOGFILE);
##
## Check the Mastership of a Branch
##
      if ($replica ne $MASTERSHIP){
         $failflag = system("$ReqMaster .@@/main");
         if ($failflag){
            brcmlib::Prompt("*** ERROR at reqmaster .@@/main");
            exit(1);
         };
      }; 
   }; #- Completed Process ISCHECKED

}; ## Completed Process Parent Directory for ($FILE_PN)
  ## This will creates a checked in new element with an empty version 0
  ## It assigs mastership of the element's main branch to the VOB replica
  ## in which you executed the mkelem command.

  ## Do Rename for the New Directory
  if (-d $FILE_PN){
     $FILE_PN =~ tr/\\/\//;
     my @tmp_buffer = split('/',$FILE_PN);
     @tmp_buffer = reverse(@tmp_buffer);
     $FILE_PN = shift(@tmp_buffer);
     my $tmpFILE_PN = $FILE_PN.".keep";
     $failflag = system("rename $FILE_PN $tmpFILE_PN");
     if ($failflag){
         brcmlib::Prompt("*** ERROR ($failflag): rename $FILE_PN $tmpFILE_PN");
         exit(1);
     };
     $failflag = system("cleartool mkelem -c \"Adding new directory\" -eltype directory -nco -master $FILE_PN");
     if ($failflag){
         brcmlib::Prompt("*** ERROR ($failflag): at mkelem for directory $FILE_PN");
	 $failflag = system("rename  $tmpFILE_PN $FILE_PN");
     	 if ($failflag){
         	brcmlib::Prompt("*** ERROR ($failflag): rename $tmpFILE_PN $FILE_PN");
         	exit(1);
     	 };
         exit(1);
     };
     $failflag = system("xcopy /Q/I/E $tmpFILE_PN $FILE_PN");
     if ($failflag){
         brcmlib::Prompt("*** ERROR ($failflag): on xcopy /E $tmpFILE_PN $FILE_PN" );
         exit(1);
     };
     $failflag = system("rd /S/Q $tmpFILE_PN");
     if ($failflag){
         brcmlib::Prompt("*** ERROR ($failflag): on removing $tmpFILE_PN");
         exit(1);
     };
  ##
  ## Add the IR_Locked_By Attribute (for Directory)
  ##
     $failflag = system("cleartool mkattr -c \"Add IR_Locked_By attribute\" -default IR_Locked_By $FILE_PN");
     if ($failflag){
        brcmlib::Prompt("*** ERROR ($failflag): at Adding IR_Locked_By attribute on directory $FILE_PN");
        exit(1);
     };
  } else { #- (for file)
     $failflag = system("cleartool mkelem -nco -master -nc $FILE_PN");
     if ($failflag){
         brcmlib::Prompt("*** ERROR ($failflag): at mkelem for $FILE_PN");
         exit(1);
     };
  ##
  ## Add the IR_Locked_By Attribute (for File)
  ##
     $failflag = system("cleartool mkattr -c \"Add IR_Locked_By attribute\" IR_Locked_By \\\"$USERNAME\\\" $FILE_PN");
     if ($failflag){
        brcmlib::Prompt("*** ERROR ($failflag): at Adding IR_Locked_By attribute on $FILE_PN");
        exit(1);
     };
  ##
  ## (CAll CO RESERVED Functions) ##
  ##
     $failflag = system("cleartool co -nc $FILE_PN");
     if ($failflag){
        brcmlib::Prompt("*** ERROR ($failflag): at -co $FILE_PN");
        exit(1);
     };
     #$failflag = system("rename $FILE_PN.keep $FILE_PN");
     $failflag = rename($FILE_PN.".keep",$FILE_PN);
     if (!$failflag){ #1 for success
        brcmlib::Prompt("*** ERROR ($failflag): at rename $FILE_PN.keep $FILE_PN");
        exit(1);
     };
  };
};
