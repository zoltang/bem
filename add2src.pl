#############################################################
# add2src.pl                                                #
# This pre mkelement trigger is utilized to create elements #
# on the main line of a replicated vob (non master site)    #
#############################################################
__END__
unshift(@INC,'\\\\SJCLEARCASE\\ccs_d\\Lib\\');
require Trigger::triggers;

my $SCRIPT_DIR =    "\\\\Sjclearcase\\ccs_d\\";
my $USERNAME   = lc "$ENV{'CLEARCASE_USER'}";
my $OP_NAME    =    "$ENV{'CLEARCASE_OP_KIND'}";
my $FILE_PN    =    "$ENV{'CLEARCASE_PN'}";
my $VOB_PN     =    "$ENV{'CLEARCASE_VOB_PN'}";
my $VERSION    =    "$ENV{'CLEARCASE_ID_STR'}";
my $COMMENT    =    "$ENV{'CLEARCASE_COMMENT'}";
my $VOB_OWNER  =    `cleartool desc -fmt %u -vob $VOB_PN`;
my $MASTERSHIP =    $ENV{'CLEARCASE_FREPLICA'};
my $failflag;

if (-z $FILE_PN){
   triggers::Prompt("*** WARNING $FILE_PN is zero in size");
};

## Checked out its parent directory
###$pwd = ".@@\main";
$pwd = "M:/jtarng_view/Test/sqatest/dir2/";
triggers::Prompt($pwd);
$failflag = system("cleartool co -c \"Adding new element to replica\" -unreserved -nmaster .@@\main");
triggers::Prompt("<<< $failflag >>>");
if ($failflag){
   triggers::Prompt("*** ERROR at co -unreserved -nmaster .@@\/main");
   exit(1);
};
triggers::Prompt("SUCCESS SUCCESS");

## Bring Dir mastership to replica
$failflag = system("multitool reqmaster .@@/main");
if ($failflag){
   triggers::Prompt("*** ERROR at reqmaster .@@\/main");
   exit(1);
};

## This will creates a checked in new element with an empty version 0
## It assigs mastership of the element's main branch to the VOB replica
## in which you executed the mkelem command.
$failflag = system("cleartool mkelem -nco -master -c \"$comment\" $FILE_PN");
if ($failflag){
   triggers::Prompt("*** ERROR at -nco -master -c $FILE_PN");
   exit(1);
};
## (CAll CO UNRESERVED Functions) ##
##----------------------------------
$failflag = system("cleartool co -nc $FILE_PN");
if ($failflag){
   triggers::Prompt("*** ERROR at -co $FILE_PN");
   exit(1);
};
triggers::Prompt("DEBUG DEBUG DEBUG---1");

$failflag = system("mv $FILE_PN.keep $FILE_PN");
triggers::Prompt("DEBUG DEBUG DEBUG---2");
exit(0);
if ($failflag){
   triggers::Prompt("*** ERROR at mv $FILE_PN.keep $FILE_PN");
};

## (CAll CI Function that validates mastership) ##
##------------------------------------------------
$failflag = system("cleartool ci -nc $filename");
if ($failflag){
   triggers::Prompt("*** ERROR (validates mastership) at ci $filename");
};

## (CAll CI Function that validates mastership) ##
##------------------------------------------------
$failflag = system("cleartool ci -nc .");
if ($failflag){
   triggers::Prompt("*** ERROR (validates mastership) at ci .");
};
  
