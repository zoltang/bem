#--------------------------------------------------------#
# Add in Locked_By attribute for a newly created element #
# 1. It is triggered by a post-mkelem action             #
# 2. Locked_By attribute type is assumed already existed #
# Author: James Tarng                                    #
# e-mail: jtarng@broadcom.com                            #
#--------------------------------------------------------#
__END__
my $failflag;
$failflag = system("cleartool mkattr -default Locked_By $ENV{'CLEARCASE_PN'}");
if ($failflag != 0){
   system("dir > d:/Temp/error.txt");
};
__END__

#---DEBUG START
my $tmpfile = "d:/Temp/log.txt";
system("dir > d:/Temp/trigger.txt");

open(ERRLOG,">> $tmpfile")||die "*** ERROR: Can not open $tmpfile\n";
print ERRLOG "---------------\n";
print ERRLOG "-- $ENV{'CLEARCASE_PN'} --\n";
print ERRLOG "---------------\n";
close(ERRLOG);
#---DEBUG END

