# Set Up User Account automatically.
# Some stuff
use CQPerlExt;
require Net::SMTP;
require 5.001;


my $logon = $ARGV[0];
chomp $logon;

#Create a ClearQuest admin session
$adminSession = CQPerlExt::CQAdminSession_Build();
#Logon as admin
$adminSession->Logon("admin","3rn3st","");
#Create the user object

$find_user = $adminSession->GetUser($logon);
if ($find_user) {
    print "$userName already defined \n";
} else {
    print "Adding $userName.. \n";
    $newUserObj = $adminSession->CreateUser($logon);
    $find_user = $newUserObj;
    UpdateUser();
    sendmail();
}

sub UpdateUser {
		$find_user->UnsubscribeAllDatabases();
		$dbObj1 = $adminSession->GetDatabase( "CM" );
		$dbObj2 = $adminSession->GetDatabase( "TEST5" );
		#Subscribe to the two required databases.
		print "subscribing to CM";
		$find_user->SubscribeDatabase($dbObj1);
		print "subscribing to TEST5";
		$find_user->SubscribeDatabase($dbObj2);
		$newUserObj->SetLDAPAuthentication($logon);
}

sub sendmail {
 		# A quick fix below to get it to cc user! 
 		my $subj="New ClearQuest Account Set-up \n";
 		my $body1="Your ClearQuest account has been set-up the login details are the same as your network login \n";
		my $body2="Please login to ClearQuest at the following URL...  \n";
		my $body3="\n https://dev-blade-f37/cqweb/login\n";
		my $body4="Once you've logged on please add your fullname and email address by clicking on the User Profile button \n";
 		$smtp = Net::SMTP->new("dev-relay-1"); 		# connect to an SMTP server
 		$smtp->mail("configuration.management\@cmcmarkets.com");	# use the sender's address here
 		$smtp->to("$logon\@cmcmarkets.com");        			# recipient's address
 		$smtp->data();         			        # Start the mail
 		# Send the header.
 		$smtp->datasend("To: $logon\@cmcmarkets.com\n");
 		$smtp->datasend("From: configuration.management\@cmcmarkets.com\n");
 		$smtp->datasend("Subject: $subj\n");
 		# Send the body.
 		$smtp->datasend("$body1\n");
 		$smtp->datasend("$body2\n");
 		$smtp->datasend("$body3\n");
 		$smtp->datasend("$body4\n");	
 		$smtp->dataend();                   # Finish sending the mail
		$smtp->quit;                        # Close the SMTP connection
} 
